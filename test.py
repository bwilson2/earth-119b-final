#%%
import numpy as np
from skimage.io import imread, imshow
import matplotlib.pyplot as plt
from matplotlib.patches import Circle, Rectangle
from skimage import transform
from skimage.color import rgb2gray
from skimage.feature import match_template
from skimage.feature import peak_local_max
from skimage.transform import rescale, resize, downscale_local_mean
from skimage.util.dtype import convert
from glob import glob
from skimage import io
import os

from joblib import Parallel, delayed

# cwd = os.getcwd()
# #path = cwd + "/maskhm"
# os.chdir(cwd)


# imgs = []
# imgs_list = os.listdir()
# imgs_list.sort()
# for i in range(2):
#     img = imread(imgs_list[i], as_gray=True)
  
#     imgs.append(img)

# templateLoad = imread('/Users/brendan/Desktop/mask project/maskhm/IMG_0610 Small.jpg')
# templateBW = rgb2gray(templateLoad)
# template = templateBW[120:215,70:160]
# img1 = imgs[0]

# def MaskFinder(img):
#     resulting_image = match_template(imgs,template)
#     x, y = np.unravel_index(np.argmax(resulting_image), resulting_image.shape)
#     template_width, template_height = template.shape
#     for x, y in peak_local_max(resulting_image], threshold_abs=0.65, exclude_border = 20):
#         rect = plt.Rectangle((y, x), template_height, template_width,color='r', fc='none')
#         plt.gca().add_patch(rect)


# if __name__ == '__main__':
#     MaskFinder()
#     Parallel(n_jobs=1, backend="threading")(delayed(MaskFinder)(img) for img in imgs)



#plt.savefig("Test.jpg")

# %%
